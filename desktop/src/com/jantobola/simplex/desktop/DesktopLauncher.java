package com.jantobola.simplex.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jantobola.simplex.CoreMain;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1280;
		config.height = 720;
		config.samples = 8;
		config.title = "Prism Worlds";

		new LwjglApplication(new CoreMain(), config);
	}
}
