package com.jantobola.simplex;

import com.jantobola.box2d.CharacterStateData;
import com.uwsoft.editor.renderer.Overlap2DStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.resources.ResourceManager;

public class GameStage extends Overlap2DStage {

	private ResourceManager rm;

	public GameStage(ResourceManager rm) {
		super();
		this.rm = rm;

		initSceneLoader(rm);
		sceneLoader.loadScene("MainScene");
		addActor(sceneLoader.getRoot());

		getCharacter().getBody().setUserData(new CharacterStateData());

	}

	public CompositeItem getRoot() {
		return sceneLoader.getRoot();
	}

	public CompositeItem getCharacter() {
		return getRoot().getCompositeById("character");
	}

	public CharacterStateData getCharacterStateData() {
		return (CharacterStateData) getCharacter().getBody().getUserData();
	}

}
