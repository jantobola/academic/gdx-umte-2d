package com.jantobola.simplex;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.jantobola.box2d.CharacterStateData;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;
import com.uwsoft.editor.renderer.resources.ResourceManager;

public class CoreMain extends ApplicationAdapter {

	private boolean debug = false;

	private GameStage stage;
	private ResourceManager rm;

	private Box2DDebugRenderer debugRenderer;
	private Matrix4 debugMatrix;

	private Body character;

	private float DEF_SPEED = 40;
	private float DEF_ACCELERATION = 25;
	private float DEF_JUMP = 80;

	FPSLogger log = new FPSLogger();

	private float amountSpeed = DEF_SPEED;
	private float amoutAcceleration = DEF_SPEED + DEF_ACCELERATION;

	@Override
	public void create() {
		rm = new ResourceManager();
		rm.initAllResources();

		stage = new GameStage(rm);

		character = stage.getCharacter().getBody();

		character.setGravityScale(-9.8f);
		character.setFixedRotation(true);
		character.setLinearVelocity(0, 0);
		character.setUserData(new CharacterStateData());

		stage.getWorld().setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				Vector2 collisionNormal = contact.getWorldManifold().getNormal();

				if(Math.abs(collisionNormal.x) < 0.8f && Math.abs(collisionNormal.y) > 0.2) {
					((CharacterStateData) character.getUserData()).isAbleToJump = true;
					amountSpeed = DEF_SPEED;
					amoutAcceleration = DEF_SPEED + DEF_ACCELERATION;
				} else {
					amountSpeed = 0;
					amoutAcceleration = 0;
				}

				((CharacterStateData) character.getUserData()).isCollision = true;
				contact.setFriction(0.5f);
			}

			@Override
			public void endContact(Contact contact) {
				((CharacterStateData) character.getUserData()).isAbleToJump = false;
				((CharacterStateData) character.getUserData()).isCollision = false;
				amountSpeed = DEF_SPEED;
				amoutAcceleration = DEF_SPEED + DEF_ACCELERATION;
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {

			}
		});

		debugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);

	}

	@Override
	public void render() {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
			amountSpeed = amoutAcceleration;
			DEF_JUMP = 90;
		} else if(((CharacterStateData) character.getUserData()).isAbleToJump) {
			amountSpeed = DEF_SPEED;
			DEF_JUMP = 80;
		}

		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT)) {

			Vector2 currVelocity = character.getLinearVelocity();

			float velocityChange = -amountSpeed - currVelocity.x;
			float impulse = character.getMass() * velocityChange;
			character.applyLinearImpulse(impulse, 0, character.getWorldCenter().x, character.getWorldCenter().y, true);
		}

		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT)) {

			Vector2 currVelocity = character.getLinearVelocity();

			float velocityChange = amountSpeed - currVelocity.x;
			float impulse = character.getMass() * velocityChange;
			character.applyLinearImpulse(impulse, 0, character.getWorldCenter().x, character.getWorldCenter().y, true);
		}

		if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_UP) && ((CharacterStateData) character.getUserData()).isAbleToJump) {
			character.applyLinearImpulse(0, DEF_JUMP, character.getPosition().x, character.getPosition().y, true);
		}

		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_DOWN)) {
			character.applyLinearImpulse(0, -4, character.getPosition().x, character.getPosition().y, true);
		}

		if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
			if(Gdx.input.isKeyJustPressed(Input.Keys.R)) {
				character.setTransform(stage.getCharacterStateData().spawnPosition, 0);
			}
		}

		if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
			if(debug) {
				debug = false;
			} else {
				debug = true;
			}
		}

		((OrthographicCamera) stage.getCamera()).position.x = stage.getCharacter().getX();
		((OrthographicCamera) stage.getCamera()).position.y = stage.getCharacter().getY();

		stage.act();
		stage.draw();

		if(debug) {
//			stage.setDebugAll(true);
			float BOX_TO_WORLD = 1 / PhysicsBodyLoader.SCALE;
			debugMatrix = stage.getCamera().combined;
			debugMatrix.scale(BOX_TO_WORLD, BOX_TO_WORLD, 0);
			debugRenderer.render(stage.getWorld(), debugMatrix);
			System.out.println("[H] - DEBUG MODE ACTIVE");
		}

		log.log();

	}

}
