package com.jantobola.simplex;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;

public class SimplexGame extends ApplicationAdapter {

	public OrthographicCamera cam;
	Controller control;

	public Mesh prism;
	public Model worldFragment;
	public Array<ModelInstance> modelInstances = new Array<ModelInstance>();
	public ModelBatch batch;

	@Override
	public void create() {

// ----------- Triangular Prism Mesh -----------

		prism = new Mesh(true, 6, 24,
				new VertexAttributes(
						new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
						new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
				)
		);

		Material material = new Material(
				IntAttribute.createCullFace(Gdx.gl.GL_BACK),
				ColorAttribute.createAmbient(Color.WHITE)
		);

		float a = 1f;

		float x12 = a/2;
		float z12 = (float) Math.sqrt(1f / 12) * a;
		float z3 = (float) Math.sqrt(3) / -3f * a;

		prism.setVertices(new float[] {
				-x12, -0.5f, z12, Color.BLUE.toFloatBits(), 	// [0]
				+x12, -0.5f, z12, Color.RED.toFloatBits(), 		// [1]
				+0.0f, -0.5f, z3, Color.GREEN.toFloatBits(), 	// [2]

				-x12, +0.5f, z12, Color.BLUE.toFloatBits(), 	// [3]
				+x12, +0.5f, z12, Color.RED.toFloatBits(), 		// [4]
				+0.0f, +0.5f, z3, Color.GREEN.toFloatBits(), 	// [5]
		});

		prism.setIndices(new short[] {
			0, 2, 1,
			3, 4, 5,
			0, 1, 4,
			0, 4, 3,
			1, 2, 5,
			1, 5, 4,
			2, 0, 3,
			2, 3, 5
		});

		ModelBuilder builder = new ModelBuilder();

		builder.begin();
		builder.part("prism", prism, Gdx.gl.GL_TRIANGLES, material);
		worldFragment = builder.end();

		modelInstances.add(new ModelInstance(worldFragment));
		modelInstances.get(0).transform.rotate(0, 0, 1, 90);
		modelInstances.get(0).transform.rotate(0, 1, 0, 90);
		modelInstances.get(0).transform.scale(1, 10, 1);

		batch = new ModelBatch();

// ------------ camera -------------

		cam = new OrthographicCamera(20, 20);
		cam.position.set(0f, 0f, 20f);
		cam.near = 1;
		cam.far = 40;
		cam.update();

		control = new Controller(cam, this);
		Gdx.input.setInputProcessor(control);
	}

	@Override
	public void render() {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		control.update();

		batch.begin(cam);
		batch.render(modelInstances);
		batch.end();

	}
}
