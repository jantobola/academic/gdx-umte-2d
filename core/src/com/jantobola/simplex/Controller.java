package com.jantobola.simplex;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;

public class Controller extends CameraInputController implements InputProcessor {

	private SimplexGame game;

	public Controller(Camera camera, SimplexGame game) {
		super(camera);
		this.game = game;
	}

	@Override
	public boolean keyTyped(char character) {

		switch (character) {
			case ' ':
				game.modelInstances.get(0).transform.rotate(0, 1, 0, 120);
				return true;
		}

		return false;
	}

}
