package com.jantobola.box2d;

import com.badlogic.gdx.math.Vector2;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;

public class CharacterStateData {

	public Vector2 spawnPosition = new Vector2(110 * PhysicsBodyLoader.SCALE, 200 * PhysicsBodyLoader.SCALE);

	public boolean isCollision = false;
	public boolean isAbleToJump = false;

	public CharacterStateData() {

	}
}
